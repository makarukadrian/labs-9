package pk.labs.Lab9;

import pk.labs.Lab9.beans.*;
import pk.labs.Lab9.beans.impl.Consultation1;
import pk.labs.Lab9.beans.impl.ConsultationList1;
import pk.labs.Lab9.beans.impl.ConsultationListFactory1;
import pk.labs.Lab9.beans.impl.Term1;

public class LabDescriptor {
    
    public static Class<? extends Term> termBean = Term1.class;
    public static Class<? extends Consultation> consultationBean = Consultation1.class;
    public static Class<? extends ConsultationList> consultationListBean = ConsultationList1.class;
    public static Class<? extends ConsultationListFactory> consultationListFactory = ConsultationListFactory1.class;
    
}
