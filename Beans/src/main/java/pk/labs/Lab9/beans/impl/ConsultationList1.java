/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.Lab9.beans.impl;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.beans.VetoableChangeSupport;
import java.io.Serializable;
import java.util.Date;
import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.ConsultationList;
import pk.labs.Lab9.beans.impl.Consultation1;

/**
 *
 * @author st
 */
public class ConsultationList1 implements ConsultationList, Serializable
{
    private PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    private VetoableChangeSupport vcs = new VetoableChangeSupport(this);
    private int minDuration;
    private int maxDuration;
    private Date begin;
    private Date end;
    private Consultation[] cList;
    
    public ConsultationList1(){
        cList = new Consultation[0];
    }
    
    @Override
    public int getSize() {
        return cList.length;
    }

    @Override
    public Consultation[] getConsultation() {
        return this.cList;
    }

    @Override
    public Consultation getConsultation(int index) {
        if (index < this.cList.length) {
            return this.cList[index];
        }
        return this.cList[0];
    }

    @Override
    public void addConsultation(Consultation consultation2) throws PropertyVetoException {
       for (int i = 0; i < this.cList.length; i++) 
        {
//            if (consultation2.getBeginDate().before(this.consultation[i].getEndDate())||this.consultation[i].getBeginDate().before(consultation2.getEndDate())) 
            if ((consultation2.getEndDate().after(this.cList[i].getBeginDate())&&consultation2.getBeginDate().before(this.cList[i].getBeginDate()))||(consultation2.getBeginDate().before(this.cList[i].getEndDate())&&consultation2.getEndDate().after(this.cList[i].getBeginDate()))) 
            {
                throw new PropertyVetoException("cList",null);
            }
        }
        
        Consultation[] stare = this.cList;
        Class typ = this.cList.getClass().getComponentType();
        int rozmiar = java.lang.reflect.Array.getLength(this.cList);
        Object nowe = java.lang.reflect.Array.newInstance(typ, rozmiar+1);
        this.cList = (Consultation[])nowe;
        this.cList[this.cList.length - 1] = consultation2;
        pcs.firePropertyChange("consultation", stare, this.cList); 
        
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        this.pcs.removePropertyChangeListener(listener);
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        this.pcs.addPropertyChangeListener(listener);
    }
    public void
    addVetoableChangeListener(VetoableChangeListener listener) {
        vcs.addVetoableChangeListener(listener);
    }
    
    public void
    removeVetoableChangeListener(VetoableChangeListener listener) {
        vcs.removeVetoableChangeListener(listener);
    }
}
