/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.Lab9.beans.impl;

import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Date;
import pk.labs.Lab9.beans.Term;


/**
 *
 * @author st
 */
public class Term1 implements Term, Serializable
{
    private Date begin;
    private int duration;
    private PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    
    @Override
    public Date getBegin() {
        return this.begin;
    }

    @Override
    public void setBegin(Date begin) {
        Date stare = this.begin;
        this.begin = begin;
        pcs.firePropertyChange("begin", stare, this.begin);
   }

    @Override
    public int getDuration() {
        return this.duration;
    }

    @Override
    public void setDuration(int duration) {
        if (duration > 0) {
           int stare = this.duration;
           this.duration = duration;
           pcs.firePropertyChange("duration", stare, this.duration); 
        }
        
    }

    @Override
    public Date getEnd() {
        return new Date(this.begin.getTime() + this.duration * 60 * 1000);
    }

    
}

