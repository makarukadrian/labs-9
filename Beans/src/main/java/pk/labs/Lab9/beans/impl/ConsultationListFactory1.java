/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.Lab9.beans.impl;

import java.beans.PropertyVetoException;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.ConsultationList;
import pk.labs.Lab9.beans.ConsultationListFactory;

/**
 *
 * @author st
 */
public class ConsultationListFactory1 implements ConsultationListFactory
{
    @Override
    public ConsultationList create() {
        return new ConsultationList1();
    }

    @Override
    public ConsultationList create(boolean deserialize) {
        ConsultationList consultationList= new ConsultationList1();
        if (deserialize) {
            try {
                XMLDecoder dec = new XMLDecoder(new BufferedInputStream(new FileInputStream("lab9.xml"))); 
                    consultationList.addConsultation((Consultation)dec.readObject());
                dec.close();
            } catch (    FileNotFoundException | PropertyVetoException ex) {
                Logger.getLogger(ConsultationListFactory1.class.getName()).log(Level.SEVERE, null, ex);
            }
            return consultationList;
        }
        else{
           return create();
        }
    }

    @Override
    public void save(ConsultationList consultationList) {
        try {
            try (XMLEncoder enc = new XMLEncoder(new BufferedOutputStream(new FileOutputStream("lab9.xml")))) {
                enc.writeObject(consultationList.getConsultation()[0]);
                enc.close();
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ConsultationListFactory1.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
