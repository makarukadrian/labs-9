/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.Lab9.beans.impl;

import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.beans.VetoableChangeSupport;
import java.io.Serializable;
import java.util.Date;
import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.Term;

/**
 *
 * @author st
 */
public class Consultation1 implements Consultation, Serializable
{
    private PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    private VetoableChangeSupport vcs = new VetoableChangeSupport(this);
    private String student;
    private Term term;

    @Override
    public String getStudent() {
    return this.student;    
    }

    @Override
    public void setStudent(String student) {
        String stare = this.student;
        this.student = student;
        pcs.firePropertyChange("student", stare, this.student);
    }

    @Override
    public Date getBeginDate() {
        return this.term.getBegin();
    }

    @Override
    public Date getEndDate() {
        return this.term.getEnd();
    }
    public Term getTerm(){
    return this.term;
}
    @Override
    public void setTerm(Term term) throws PropertyVetoException {
        Term stare = this.term;
        vcs.fireVetoableChange("term", stare, term);
        this.term = term;
        pcs.firePropertyChange("term", stare, this.term);
    }

    @Override
    public void prolong(int minutes) throws PropertyVetoException {
        if (minutes <=0)minutes = 0; 
            int stare = this.term.getDuration();
            vcs.fireVetoableChange("term", stare, stare + minutes);
            this.term.setDuration(this.term.getDuration() + minutes);
            pcs.firePropertyChange("term", stare, this.term.getDuration());
        
    }   
    public void addVeto(VetoableChangeListener vcs)
    {
     this.vcs.addVetoableChangeListener(vcs);
    }
}


